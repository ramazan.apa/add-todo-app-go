package handler

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/ramazan.apa/add-todo-app-go/service"
)

func GetHTTPHandler() *mux.Router {

	r := mux.NewRouter()

	cors := handlers.CORS(
		handlers.AllowedHeaders([]string{"content-type"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowCredentials(),
	)

	r.Use(cors)

	api := r.PathPrefix("/api").Subrouter()

	api.
		Methods("POST", "OPTIONS").
		Path("/add-todo").
		HandlerFunc(service.AddTodo)

	api.
		Methods("GET", "OPTIONS").
		HandlerFunc(service.GetTodos)

	return api
}
