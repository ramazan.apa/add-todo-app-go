//+build pact

package handler

import (
	"fmt"
	"gitlab.com/ramazan.apa/add-todo-app-go/repository"
	"gitlab.com/ramazan.apa/add-todo-app-go/service"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"gitlab.com/ramazan.apa/add-todo-app-go/model"
	//"strconv"
)

func TestPactProvider(t *testing.T) {



	go startInstrumentedProvider()

	pact := createPact()

	//go startServer()

	// Verify the Provider - Tag-based Published Pacts for any known consumers
	baseurl := fmt.Sprintf("http://localhost:%d", port)
	log.Printf("baseurl %s",baseurl)
	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL: baseurl,
		//PactURLs:                   []string{"https://ramazan.pactflow.io/pacts/provider/provider/consumer/consumer"},
		BrokerURL:                  "https://ramazan.pactflow.io",
		BrokerToken:                "ntgRrKGTpjVTFkF-nfDj6Q",
		PublishVerificationResults: true,
		ProviderVersion:            "2.0.0",
		StateHandlers: stateHandlers,
	})

	if err != nil {
		t.Log(err)
	} else {
		t.Log(err)
	}

}


var stateHandlers = types.StateHandlers{
	"todo exists": func() error {
		service.TodoRepo = movieExists
		return nil
	},
}

func startInstrumentedProvider() {

	hh := GetHTTPHandler()
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), hh))

}


var movieExists = &repository.TodoRepository{
	OurTodos: model.AddTodos{{
		Text: "Movie 1",
	}},
}

// Configuration / Test Data
var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/pacts", dir)
var logDir = fmt.Sprintf("%s/pacts/log", dir)
var port = 8089

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Provider:                 "provider",
		Consumer:                 "consumer",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "INFO",
	}
}
