# add-todo-app-go
- This Go app is a back-end and server side of modanisa assignment.This app allows you create todos and list them.

## Status of Backend Assignment
I think the project is almost complete. You can see our live backend port. Url is : "http://178.62.234.102:8080/".

<a href="https://ibb.co/vzRFR9M"><img src="https://i.ibb.co/h8w6w0T/go-live.png" alt="go-live" border="0"></a><br /><a target='_blank' href='https://tr.imgbb.com/'></a><br />

### `go run main.go`

Runs the app in the development mode.
Open "http://localhost:8080" to view it in the browser.

### Unit Tests
I wrote unit tests using Go's built-in test package.

### Contract Tests
-Contract tests wrotes with `pact` tool. Pact.io has a tool called `pact-flow` publishing and verifying pacts between provider and consumer


### `go test ./... -tags=unit`
Run the unit tests with this command

### `go test ./... -tags=pact`
Run the pact tests with this command

It's like :

<a href="https://ibb.co/QHjYXvv"><img src="https://i.ibb.co/fNGrQdd/go-pact-test.png" alt="go-pact-test" border="0"></a>

And on pact-flow io :

<a href="https://ibb.co/F64QFjZ"><img src="https://i.ibb.co/0qy0xd4/pact-go-test-final.png" alt="pact-go-test-final" border="0"></a>

## CI/CD
I used Gitlab pipeline for this project. Before I installed my local docker GoLang image to digital ocean server. 
It gives me a server url and i used this url on my gitlab-cli.yml

<a href="https://ibb.co/JyYtgHQ"><img src="https://i.ibb.co/TbQ2dtL/go-ocean-server.png" alt="go-ocean-server" border="0"></a>

<a href="https://ibb.co/dPZ1q3Y"><img src="https://i.ibb.co/v39gGrn/go-pipeline.png" alt="go-pipeline" border="0"></a><br /><a target='_blank' href='https://tr.imgbb.com/'></a><br />

When i pushed any code to my relevant branch gitlab-cli.yml works and updates my url. 













