module gitlab.com/ramazan.apa/add-todo-app-go

go 1.17

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/pact-foundation/pact-go v1.6.4
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/hashicorp/go-version v1.3.0 // indirect
	github.com/hashicorp/logutils v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
