package service

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/ramazan.apa/add-todo-app-go/model"
	"gitlab.com/ramazan.apa/add-todo-app-go/repository"
)

type Return struct {
	Todos interface{} `json:"todos"`
}

var TodoRepo = &repository.TodoRepository{
	OurTodos: model.AddTodos{},
}

func GetTodos(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	res := Response{w}
	res.SendJSON(TodoRepo.GetAddTodos())

}

func AddTodo(w http.ResponseWriter, r *http.Request) {
	var todo1 model.AddTodo
	res := Response{w}
	_return := &Return{}
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &todo1); err != nil {
		panic(err)
	}

	todoRes := TodoRepo.CreateAddTodos(todo1)

	_return.Todos = todoRes
	res.SendJSON(_return)
}
