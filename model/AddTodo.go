package model

type AddTodo struct {
	Id   int    `json:"id"`
	Text string `json:"text"`
}

type AddTodos []AddTodo

type AddTodoRequest struct {
}

type AddTodoResponse struct {
}

type AddTodosRepos struct {
	OurTodos AddTodos
}
