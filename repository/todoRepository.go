package repository

import (
	. "gitlab.com/ramazan.apa/add-todo-app-go/model"
)

type TodoRepository struct {
	CurrentID int
	OurTodos  AddTodos
}

func (at *TodoRepository) GetAddTodos() AddTodos {
	res := AddTodos{}
	res = append(res, at.OurTodos...)

	return res
}

func (at *TodoRepository) CreateAddTodos(a AddTodo) AddTodo {
	at.CurrentID++
	a.Id = at.CurrentID
	at.OurTodos = append(at.OurTodos, a)
	return a

}
